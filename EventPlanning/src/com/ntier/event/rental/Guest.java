package com.ntier.event.rental;
/**
 * 
 * @author Bhupesh shrestha
 * Date: 9/17/2020
 * Lab 10.3 Guest Bean
 *
 */

public class Guest {

	private String firstName;
	private String lastName;
	private String hotelClubNumber;
	private String rentalCarClubNumber;
	
	//first constructor
	public Guest(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	//second constructor
	public Guest(String firstName, String lastName, String hotelClubNumber, String rentalCarClubNumber) {
		this(firstName,lastName);
		this.hotelClubNumber = hotelClubNumber;
		this.rentalCarClubNumber = rentalCarClubNumber;
	}
	
	public Guest(String hotelClubNumber) {
		this.hotelClubNumber = hotelClubNumber;
	}

	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getHotelClubNumber() {
		return hotelClubNumber;
	}
	
	public void setHotelClubNumber(String hotelClubNumber) {
		this.hotelClubNumber = hotelClubNumber;
	}
	
	public String getRentalCarClubNumber() {
		return rentalCarClubNumber;
	}
	
	public void setRentalCarClubNumber(String rentalCarClubNumber) {
		this.rentalCarClubNumber = rentalCarClubNumber;
	}
	
}

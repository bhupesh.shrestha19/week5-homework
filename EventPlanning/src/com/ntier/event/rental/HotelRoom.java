package com.ntier.event.rental;
/**
 * 
 * @author Bhupesh shrestha
 * Date: 09/17/2020
 * Lab 8.1 Data Members in Classes
 * Lab 8.2 Accessor Methods 
 * Lab 8.3 Representing an Object's Data
 * Lab 10.2 Constructor
 * 
 */

public class HotelRoom {

	private int roomNumber;
	private boolean isSmoking;
	
	//for lab 10.2 constructor
//	public HotelRoom(int roomNumber) {
//		this.setRoomNumber(roomNumber);
//	}
	
	public int getRoomNumber() {
		return roomNumber;
	}
	
	public void setRoomNumber(int roomNumber) {
		this.roomNumber = roomNumber;
	}
	
	public boolean isSmoking() {
		return isSmoking;
	}
	
	public void setSmoking(boolean isSmoking) {
		this.isSmoking = isSmoking;
	}

}

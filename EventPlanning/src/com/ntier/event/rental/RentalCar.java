package com.ntier.event.rental;
/**
 * 
 * @author Bhupesh shrestha
 * Date: 09/17/2020
 * Lab 8.1 Data Members in Classes
 * Lab 8.2 Accessor Methods 
 * Lab 8.3 Representing an Object's Data
 * Lab 10.2 Constructor
 * 
 */

public class RentalCar {
	
	private long id;
	private String make;
	private String model;
	private String licenseNumber;
	
	//for lab 10.2 constructor
//	public RentalCar(long id) {
//		this.setId(id);
//	}
	
	public RentalCar(String licenseNumber) {
		super();
		this.licenseNumber = licenseNumber;
	}
	
	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getMake() {
		return make;
	}
	
	public void setMake(String make) {
		this.make = make;
	}
	
	public String getModel() {
		return model;
	}
	
	public void setModel(String model) {
		this.model = model;
	}


}

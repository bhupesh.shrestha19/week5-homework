package com.ntier.event.reservation;

import java.util.Scanner;
import com.ntier.event.rental.Guest;

/**
 * 
 * @author Bhupesh Shrestha
 * Date: 9/18/2020
 * Lab 11.1 Reservations
 *
 */

public class HotelReservation extends Guest {

	public HotelReservation(String hotelClubNumber) {
		super(hotelClubNumber);
  
		
		if(hotelClubNumber.charAt(0) == 'N' && hotelClubNumber.length() >= 7) {
			System.out.println("The club number is valid");
		}
		else {
			System.out.println("Not a valid number");
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
        
		HotelReservation rv = new HotelReservation("N123546");
	
	}

}

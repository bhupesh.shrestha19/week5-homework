package com.ntier.event.reservation;

import com.ntier.event.rental.RentalCar;

/**
 * 
 * @author Bhupesh shrestha
 * Date: 9/18/2020
 * Lab 11.1 Reservations
 *
 */

public class CarReservation extends RentalCar{
	
	public CarReservation(String licenseNumber) {
		super(licenseNumber);
		
		if(licenseNumber.length() >= 11) {
			System.out.println("Thanks for entering the license number.");
		}
		else {
			System.out.println("Valid license number contains at least 12 characters.");
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		CarReservation cr = new CarReservation("Mlv234");

	}

}

package com.ntier.event.test;
import com.ntier.event.rental.HotelRoom;

/**
 * 
 * @author Bhupesh shrestha
 * Date: 09/17/2020
 * Lab 8.1 Data Members in Classes
 * Lab 8.2 Accessor Methods 
 * Lab 8.3 Representing an Object's Data
 * Lab 10.2 Constructor
 * 
 */

public class HotelRoomClient {

	public static void main(String[] args) {
		//for lab 8.1
		
 //       HotelRoom room1 = new HotelRoom(); 
 //       room1.roomNumber = 12;
 //       room1.isSmoking = true;
        
 //		System.out.println("HotelRoom: room number " + room1.roomNumber + "," + " smoking preference = " 
 //		+ room1.isSmoking);
		
		// for lab 8.2
		//using getters and setters
		HotelRoom room1 = new HotelRoom(); 
		room1.setRoomNumber(12);
		room1.setSmoking(true);
		
		//for lab 10.2
	//	HotelRoom room2 = new HotelRoom(12); 
		
		// for lab 8.3 using getMethod()
		System.out.println("HotelRoom: room number " + room1.getRoomNumber() + "," + " smoking preference = " 
				 	+ room1.isSmoking());
	}
}

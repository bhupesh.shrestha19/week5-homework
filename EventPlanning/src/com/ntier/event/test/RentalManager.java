package com.ntier.event.test;
/**
 * 
 * @author Bhupesh shrestha
 * Date: 09/17/2020
 * Lab 8.3 Representing an Object's Data
 * Lab 10.5 Class-Wide Methods
 * 
 */

public class RentalManager {
	
	private static String [] car = {"Toyota Matrix, id=1, year=2009", "Honda Civic, id=2, year =2016", "Nissan Quest, id=3, year=2010", "Ford Explorer, id=4, year=2016", "Mazda Mazda3, id=5, year=2017"};
	
    private static String [] hotel = {"Room 16, smoking, bed=1", "Room 12, non-smoking, bed=2", "Room 4, smoking, bed=1", "Room 42, non-smoking, bed=2"};
    

    //method to display all cars
    public static void displayCars() {
    	for(String Car: car) {
    		System.out.println(Car);
    	}
    }
    
    //method to display all rooms
    public static void displayRooms() {
    	for(String Room: hotel) {
    		System.out.println(Room);
    	}
    }
    
	public static void main(String[] args) {
		
		//calling displayCars and displayRooms method to display all cars and rooms
	//	RentalManager rent = new RentalManager();
		//rent.displayCars();
		//rent.displayRooms();

	}

}

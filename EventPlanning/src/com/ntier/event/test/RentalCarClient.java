package com.ntier.event.test;
import com.ntier.event.rental.RentalCar;

/**
 * 
 * @author Bhupesh shrestha
 * Date: 09/17/2020
 * Lab 8.1 Data Members in Classes
 * Lab 8.2 Accessor Methods 
 * Lab 8.3 Representing an Object's Data
 * Lab 10.2 Constructor
 * 
 */

public class RentalCarClient {

	public static void main(String[] args) {
		// for lab 8.1
		
 //       RentalCar car1 = new RentalCar(); 
 //       car1.id = 123l;
 //       car1.make = "Nissan";
 //       car1.model = "Maxima";
        
 //		System.out.println("RentalCar: id=" + car1.id + " make=" + car1.make 
 // 				+ " model=" + car1.model);
 //	}
		
		//for lab 8.2
		//using getters and setters
//		RentalCar car2= new RentalCar();
//		car2.setId(123l);
//		car2.setMake("Nissan");
//		car2.setModel("Maxima");
		
		//for lab 10.2
	//	RentalCar car2= new RentalCar(123l);
		
		// for lab 8.3 using getMethod()
//		System.out.println("RentalCar: id=" + car2.getId() + " make=" + car2.getMake() 
//					+ " model=" + car2.getModel());
	}		
		
}

